﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
//including this for the extra definition
using PassionProject.Models.ViewModels;
using System.Diagnostics;

namespace PassionProject.Controllers
{
    public class ReviewController : Controller
    {
        private dbase db = new dbase();

        // GET: Review
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //This function needs to print out a list of the reviews
            //However, we also have to include the info for the item on each review
            IEnumerable<Review> reviews = db.Reviews.ToList();
            return View(reviews);
        }

        public ActionResult New()
        {
            //Connection to DB needed to grant list of list
            //ask user to pick which list wrote the review

            ReviewEdit revieweditview = new ReviewEdit();
            revieweditview.items = db.Items.ToList();

            return View(revieweditview);
        }

        [HttpPost]
        public ActionResult Create(string ReviewTitle_New, int ReviewItem_New, string ReviewBio_New)
        {
            //Standard query representation   
            string query = "insert into reviews (ReviewTitle, ReviewBio, item_ItemID) values (@title, @bio, @item)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@title", ReviewTitle_New);
            myparams[1] = new SqlParameter("@bio", ReviewBio_New);
            myparams[2] = new SqlParameter("@item", ReviewItem_New);
            //forcing the review to have an item

            //putting that information into the database by running a command
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        public ActionResult Edit(int id)
        {
            //For edit we need a list of items to pick from
            //we also need to know what the current item is

            ReviewEdit revieweditview = new ReviewEdit();
            revieweditview.items = db.Items.ToList();
            //Line equivalent to "Select * from reviews where reviewid = .."
            revieweditview.review = db.Reviews.Find(id);

            return View(revieweditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string ReviewTitle, int ReviewItem, string ReviewBio)
        {
            //If the ID doesn't exist or the review doesn't exist
            if ((id == null) || (db.Reviews.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update reviews set ReviewTitle=@title, ReviewBio=@bio, item_ItemID=@item where reviewid=@id";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@title", ReviewTitle);
            myparams[1] = new SqlParameter("@bio", ReviewBio); // check
            myparams[2] = new SqlParameter("@item", ReviewItem); //check
            myparams[3] = new SqlParameter("@id", id);
            //forcing the review to have an item

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        public ActionResult Show(int? id)
        {
            //If the id doesn't exist or the review doesn't exist
            if ((id == null) || (db.Reviews.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from reviews where reviewid=@id";
            //Could also just pass the sqlparameter by itself instead of making an array of 1 item
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            //Hey view.. here's a list of all the reviews in my db
            Review myreview = db.Reviews.SqlQuery(query, myparams).FirstOrDefault();

            return View(myreview);
        }
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Reviews.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "delete from Reviews where reviewid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return View("List");
        }

    }
}