namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemID = c.Int(nullable: false, identity: true),
                        ItemName = c.String(nullable: false, maxLength: 255),
                        ItemDetails = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.ItemID);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewID = c.Int(nullable: false, identity: true),
                        ReviewInfo = c.String(),
                        item_ItemID = c.Int(),
                    })
                .PrimaryKey(t => t.ReviewID)
                .ForeignKey("dbo.Items", t => t.item_ItemID)
                .Index(t => t.item_ItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "item_ItemID", "dbo.Items");
            DropIndex("dbo.Reviews", new[] { "item_ItemID" });
            DropTable("dbo.Reviews");
            DropTable("dbo.Items");
        }
    }
}
