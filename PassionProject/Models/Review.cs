﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Review
    {
        [Key, ScaffoldColumn(false)]
        public int ReviewID { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Title")]
        public string ReviewTitle { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Content")]
        public string ReviewBio { get; set; }

        public virtual Item item { get; set; }

    }
}