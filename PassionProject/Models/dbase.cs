﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Install "Entity Framework"
//Right click solution > Manage NuGet Packages
//Search Entity Framework and install to solution
using System.Data.Entity;

namespace PassionProject.Models
{
    public class dbase : DbContext
    {

        public dbase()
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Review> Reviews { get; set; }

    }

}