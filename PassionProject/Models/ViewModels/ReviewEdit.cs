﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject.Models.ViewModels
{
    public class ReviewEdit
    {
        //Empty constructor
        public ReviewEdit()
        {

        }

        //To edit a review, you also need to pick from a list of items

        public virtual Review review { get; set; }

        public IEnumerable<Item> items { get; set; }
    }
}