﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Item
    {
        [Key]
        public int ItemID { get; set; }

        [Required, StringLength(255), Display(Name = "Item Name")]
        public string ItemName { get; set; }

        [Required, StringLength(255), Display(Name = "Details")]
        public string ItemDetails { get; set; }

        //This is how we can represent a one (item) to many (Reviews) relation
        public virtual ICollection<Review> Reviews { get; set; }
      
    }
}